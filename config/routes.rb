Rails.application.routes.draw do
  devise_for :users

  root controller: :public, action: :index

  resources :room_messages
  resources :rooms

  # Redirect to landing page if the route isn't found
  match "*path" => redirect{|path, request| request.flash[:error] = "The page you tried to access doesn't exist."; "/"}, via: [:post, :get, :put, :patch, :delete], constraints: lambda { |req|
    # Allow active storage requests
    req.path.exclude? 'rails/active_storage'
  }
end
