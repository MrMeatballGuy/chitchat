const { environment } = require('@rails/webpacker')

// Make jQuery and popper.js available for use in other files and libs
// This is required for Bootstrap 4 to function too
const webpack = require('webpack')
environment.plugins.append(
	"Provide", 
	new webpack.ProvidePlugin({
		$: 'jquery',
		jQuery: 'jquery',
		Popper: ['popper.js', 'default']
	})
)

module.exports = environment
