class PublicController < ApplicationController
  # Displays the homepage/landing page
  def index
    if user_signed_in?
      redirect_to rooms_path 
    else
      render
    end
  end
end
