class RoomsController < ApplicationController

  before_action :authenticate_user!

  before_action :load_entities

  def index
  end

  def show
    @room_message = RoomMessage.new room: @room
    @room_messages = @room.room_messages.order(created_at: :asc).includes(:user).last(50)
  end

  protected

  def load_entities
    @rooms = Room.order_latest
    @room = Room.find(params[:id]) if params[:id]
  end
end
