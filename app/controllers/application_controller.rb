class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?

  before_action :initialize_active_storage

  # Redirect user to list of chat rooms on log in
  def after_sign_in_path_for(user)
    rooms_path
  end

  protected

  # Allow extra properties to be set on Devise user model at sign up
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:email, :username])
    devise_parameter_sanitizer.permit(:account_update, keys: [:avatar])
  end

  def initialize_active_storage
    # ActiveStorage::Current.host = request.base_url
  end
end
