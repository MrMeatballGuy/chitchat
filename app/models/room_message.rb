class RoomMessage < ApplicationRecord

  belongs_to :user
  belongs_to :room, inverse_of: :room_messages

  # Require minimum 1 character to validate a message
  validates :message, length: {minimum: 1}, allow_blank: false
  # Don't allow only whitespace in a message
  validates :message, format: {with: /.*\S.*/}

  # Add avatar URI and username to the JSON representation of a message
  def as_json(options)
    avatar = "/assets/default-avatar.png"
    if user.avatar.present?
      avatar = Rails.application.routes.url_helpers.rails_representation_url(user.avatar.variant(resize_to_limit: [128, 128]).processed, only_path: true)
    end
    super(options).merge(username: user.username, user_avatar_url: avatar)
  end
end
