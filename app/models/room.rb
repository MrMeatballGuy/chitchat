class Room < ApplicationRecord
  has_many :room_messages, dependent: :destroy, inverse_of: :room

  # Order by latest message. If a room has no messages it will be put at the end.
  scope :order_latest, -> { includes(:room_messages).order('room_messages.created_at DESC NULLS LAST') }
end
