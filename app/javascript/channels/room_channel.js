import consumer from "./consumer"

let subscription;

$(document).on("turbolinks:load", function(){

  // unsubscribe from existing action cable if one is active
  if (subscription) {
      consumer.subscriptions.remove(subscription);
      subscription = undefined;
  }
  
  // Set up the connection for action cable and define element to copy for messages
	$('[data-channel-subscribe="room"]').each(function(index, element) {
	  let $element = $(element);
	  let room_id = $element.data('room-id');
	  let messageTemplate = $('[data-role="message-template"]');
	
	  subscription = consumer.subscriptions.create(
      {
        channel: "RoomChannel",
        room: room_id
      },
      {
        received: function(data) {
          let content = messageTemplate.children().clone(true, true);
          let updated_at = data.updated_at.replace("T", " ").substring(0, data.updated_at.indexOf('.')) + " UTC";
          content.find('[data-role="user-avatar"]').attr('src', data.user_avatar_url);
          content.find('[data-role="message-username"]').text(data.username);
          content.find('[data-role="message-text"]').text(data.message);
          content.find('[data-role="message-date"]').text(updated_at);
          $element.append(content);
        }
      }
	  );
	});
})