# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Room.create([
  {
    name: "Ruby on Rails",
    description: "Chat about the Ruby on Rails framework."
  },
  {
    name: "Programming help",
    description: "Get help solving your programming questions here."
  },
  {
    name: "Chaos",
    description: "This chat has no rules enforced, post as many weird things you want."
  }
]);