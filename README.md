# README

## Requirements
* ruby 2.6.9
* node 12.22.11 
  * node 16 has issues, the "n" package can be used for global version management of node
* yarn
* redis (mac) or redis-server (linux)
* postgresql
* imagemagick (mac) or imagemagick and libmagickwand-dev (linux)

## Setup  

### Commands
run these commands in the root directory of the project
* `bundle`
* `yarn`
* `rails db:create`
* `rails db:migrate`
* `rails db:seed`

### Important note
redis and postgres must be running on your machine.  
rails expects a postgres user with the same name as the profile on your machine by default.  

